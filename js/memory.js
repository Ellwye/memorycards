var cartes = [
    "as_carreau",
    "as_carreau",
    "as_pique",
    "as_pique",
    "as_treffle",
    "as_treffle",
    "roi_coeur",
    "roi_coeur",
    "roi_treffle",
    "roi_treffle",
    "roi_pique",
    "roi_pique",
    "roi_carreau",
    "roi_carreau",
    "dame_coeur",
    "dame_coeur",
    "dame_carreau",
    "dame_carreau",
    "dame_treffle",
    "dame_treffle",
    "dame_pique",
    "dame_pique",
    "valet_pique",
    "valet_pique",
    "valet_treffle",
    "valet_treffle",
    "valet_coeur",
    "valet_coeur",
    "valet_carreau",
    "valet_carreau"
];
var cartes_random = [];
var nbCartes = cartes.length;

// Fonction qui boucle le nombre de fois de la longueur du tableau cartes
function createArrayRandom () {
    for (i=0; i < nbCartes; i++) {
        var number = Math.floor(Math.random()*cartes.length); // Génère un nombre aléatoire par rapport à la longueur du tableau cartes (pour que le nombre corresponde bien à une clé du tableau cartes)
        cartes_random.push(cartes[number]); // J'insère dans le tableau cartes_random le nombre contenu à la clé number du tableau cartes
        // console.log(cartes_random);
        cartes.splice(number, 1); // Je retire la clé correspondant à number dans le tableau cartes
    }
}
createArrayRandom();

function createCards () {
    for (i=0; i < nbCartes; i++) { // Boucle pour créer les cartes
        // Création des cartes
        $("#plateau").append($('<div class="card"><img src="img/oria.jpg" id="' + i + '" attr-value="' + cartes_random[i] + ' "class="carte-dos"><img src="img/' + cartes_random[i] + '.svg" class="carte"></div>'));
    }
}
createCards();

// Définition d'une variable avec un tableau vide
var images_return = [];
var i = 0;
var j = 0;

$(".card").on("click", function() { // Lors du click sur une carte

    var imgvalue = $(this).children('img').attr("attr-value"); // Récupérer et stocker dans la variable imgvalue la value de son enfant image
    var img = $(this).children('img:first-child'); // Récupérer et stocker dans la variable img le premier enfant image
    var img_id = $(this).children().attr("id"); // Récupérer et stocker dans la variable img_id l'id de son enfant image
    img.addClass("card_hidden"); // Ajouter la classe qui cache le dos de la carte

    // Ajouter dans le tableau images_return la carte cliquée
    images_return.push({ 
        id: img_id,
        value: imgvalue
    });

    // Si le nombre d'items dans le tableau images_return est égal au nombres d'items dans le tableau cartes_random (si toutes les paires ont été trouvées) alors lancer la fonction rejouer
    if (images_return.length == cartes_random.length) {
        rejouer();
    }
    
    // Si le nombre d'items dans le tableau images_return est égal à i +2 (= si deux cartes ont été retournées)
    if (images_return.length == (i+2)) {

        var value1 = images_return[i].value; // Stocker dans la variable la valeur de la carte retournée en premier
        var id1 = images_return[i].id; // Stocker dans la variable l'id de la carte retournée en premier
    
        var value2 = images_return[(i+1)].value; // Stocker dans la variable l'id de la carte retournée en deuxième
        var id2 = images_return[(i+1)].id; // Stocker dans la variable l'id de la carte retournée en deuxième

        if (id1 == id2) { // Si les id des deux cartes retournées sont les mêmes (si on a cliqué sur la même carte deux fois)
            images_return.splice((i+1),1); // Supprimer la carte cliquée en deuxième
        }

        if (id1 != id2) {
            j += 1;
            console.log(j);

            if (value1  != value2) { // Si les valeurs des deux cartes cliquées ne sont pas les mêmes

                // Executer une fonction après 0.5 secondes
                setTimeout(function() {
                    // Retirer la classe qui cache le dos des cartes pour qu'il s'affiche
                    $("img#" + id1).removeClass("card_hidden");
                    $("img#" + id2).removeClass("card_hidden");
                    images_return.splice(i,2); // Supprimer les deux dernières entrées dans le tableau images_return
                }, 500);
            }
    
            if (value1 == value2) { // Si les deux valeurs des cartes retournées sont les mêmes, alors une paire a été trouvée
                // Désactiver le click sur les parents des images
                // $("img#" + id1).parent().off("click");
                // $("img#" + id2).parent().off("click");
                // Ajouter une classe sur le parent des images qui le rendent inclicable
                $("img#" + id1).parent().addClass("not_clicable");
                $("img#" + id2).parent().addClass("not_clicable");
                i+=2; // Ajouter 2 à i car maintenant il y a deux cartes en plus dans le tableau
            }
        }
    }

});

function rejouer() {
    $("#succes").removeClass("invisible"); // Retire la classe qui cache la pop up de victoire
    $("#message").text("Ton score : Tu as réussi en " + (j+1) + " coups."); // Ajouter un texte personnalisé qui donne le score
};